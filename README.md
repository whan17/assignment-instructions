# Software requirements #

Ubuntu 20.04

ROS noetic

Gazebo 11

git

# Setup #

## GitLab ##
We will rely on gitlab and it is mandatory to use it for this coursework.
GitLab is an online repository that can be used with the git control revision system.
The CS department runs a GitLab hosting service, and all students should be able to access it with their CS account.
If you don't have a CS account, [register here](https://www.cs.mcgill.ca/docs/)

Important: do not share your code and repository with anyone and keep your source code secret.
If we identify that two students have identical portion of code, both will be considered to have cheated.

## Register your student id and name

First, we will need you fill up [this google form](https://forms.office.com/Pages/ResponsePage.aspx?id=cZYxzedSaEqvqfz4-J8J6kknQG5pFmJBqod1wxgyl31UQ1o2UUlFODFTQkxSVzhXVzk3QzNIR1VVMy4u)
in order for us to mark your assignments. 
You may be asked to login with your McGill credential.

## Create your own repository
We are going to be using the Git revision control system during the course.
If you use your own machine then make sure to install Git.

You will need to create a repository and use `robotic-coursework-f2022` as your repository name.  
Please make sure your repository is private.

Now, grant access to the teaching staff

![Granting the teaching staff read access](/figures/gl_permissions1.png "Granting the teaching staff read access.")

You should grant the following users *Reporter* access:
  * Hsiu-Chin LIN (username: linhz)
  * Amin SOLEIMANI ABYANEH (username: asolei2)
  * Elham DANESHMAND (username: edanes1)
  
Next, you will have to clone the repository to your local machine. You can clone the repository using either HTTPS or SSH.
Using SSH is more secure, but requires
[uploading a private key to GitLab](https://docs.gitlab.com/ee/ssh/). HTTPS is less secure but simpler as it only
requires you to enter your CS account username and password. If in doubt, HTTPS is sufficient.

In order to clone the repository via SSH you should ensure that you've uploaded a private key to GitLab, launch a terminal, and type:

```
$ git clone git@gitlab.cs.mcgill.ca:XXXXXXXX/robotic-coursework-f2022.git
```

where XXXXXXXX is your CS gitlab account id.

In order to clone the repository via HTTPS you should launch a terminal and type:

```
$ git clone https://gitlab.cs.mcgill.ca/XXXXXXXX/robotic-coursework-f2022.git
```

where XXXXXXX is your CS  gitlab account id as above, and you should be prompted to type in your CS gitlab account id and password.


## Working with git and pushing your changes

We suggest you follow the excelent [tutorial](https://www.atlassian.com/git/tutorials/what-is-version-control) from atlassian on how to use git. In particular you will need to understand the following basic meachnisms:

* [add and commit](https://www.atlassian.com/git/tutorials/saving-changes)
* [push](https://www.atlassian.com/git/tutorials/syncing/git-push)
